1) Given two dictionaries of seperate and distinct languages, return all words that are spelled the same in both languages

2) Serve the code in step 1) as a public API

3) Connect to twitter and use the API in step 2) to return all tweets in two different languages that share words
