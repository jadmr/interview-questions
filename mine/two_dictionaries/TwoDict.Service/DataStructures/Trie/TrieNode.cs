using System.Collections.Generic;

namespace TwoDict.Service.DataStructures.Trie
{
    public class TrieNode
    {
        public TrieNode(HashSet<char> alphabet)
        {
            IsEndOfWord = false;
            Children = new Dictionary<char, TrieNode>();

            foreach (var character in alphabet)
                Children.Add(character, null);
        }

        public bool IsEndOfWord { get; set; }

        public IDictionary<char, TrieNode> Children { get; private set; }
    }
}