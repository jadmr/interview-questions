namespace TwoDict.Service.DataStructures.Trie
{
    public interface IPrefixTree
    {
        void Insert(string word);
        bool IsWordInTree(string word);
    }
}