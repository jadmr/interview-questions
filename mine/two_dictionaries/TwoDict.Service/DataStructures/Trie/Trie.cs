using System.Collections.Generic;

namespace TwoDict.Service.DataStructures.Trie
{
    public class Trie : IPrefixTree
    {
        private readonly HashSet<char> alphabet;

        public Trie(HashSet<char> alphabet)
        {
            this.alphabet = alphabet;

            Root = new TrieNode(alphabet);
        }

        public TrieNode Root { get; private set; }

        public void Insert(string word)
        {
            if (string.IsNullOrWhiteSpace(word))
                return;

            var currentNode = Root;

            foreach (var key in word)
            {
                if (currentNode.Children[key] == null)
                    currentNode.Children[key] = new TrieNode(alphabet);

                currentNode = currentNode.Children[key];
            }

            currentNode.IsEndOfWord = true;
        }

        public bool IsWordInTree(string word)
        {
            if (string.IsNullOrWhiteSpace(word))
                return false;

            var currentNode = Root;

            foreach (var key in word)
            {
                if (!currentNode.Children.ContainsKey(key) || currentNode.Children[key] == null)
                    return false;

                currentNode = currentNode.Children[key];
            }

            return (currentNode != null && currentNode.IsEndOfWord);
        }
    }
}