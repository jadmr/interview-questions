using System.Collections.Generic;

namespace TwoDict.Service.Services.Alphabet
{
    public class AlphabetParser : IAlphabetParser
    {
        public HashSet<char> ParseAlphabet(IList<string> stringSet)
        {
            var alphabet = new HashSet<char>();

            //TODO: find a faster way to do this...
            foreach (var str in stringSet)
                foreach (var character in str)
                    if (!alphabet.Contains(character))
                        alphabet.Add(character);

            return alphabet;
        }
    }
}