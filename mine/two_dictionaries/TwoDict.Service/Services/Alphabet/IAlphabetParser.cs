using System.Collections.Generic;

namespace TwoDict.Service.Services.Alphabet
{
    public interface IAlphabetParser
    {
        HashSet<char> ParseAlphabet(IList<string> stringSet);
    }
}