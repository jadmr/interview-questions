using System.Collections.Generic;
using System.Linq;
using TwoDict.Service.Services.Alphabet;

namespace TwoDict.Service.Services.StringComparison
{
    public class CommonlySpelledWordFinder : ICommonlySpelledWordFinder
    {
        private readonly IAlphabetParser alphabetParser;
        private readonly IStringSetIntersectionFinder intersectionFinder;

        public CommonlySpelledWordFinder(IAlphabetParser alphabetParser, IStringSetIntersectionFinder intersectionFinder)
        {
            this.alphabetParser = alphabetParser;
            this.intersectionFinder = intersectionFinder;
        }

        public IList<string> FindCommonlySpelledWords(IList<string> firstCollectionOfWords, IList<string> secondCollectionOfWords)
        {
            if (!firstCollectionOfWords.Any() || !secondCollectionOfWords.Any())
                return new List<string>();

            // Determine the alphabet chars of each set
            var firstAlphabet = alphabetParser.ParseAlphabet(firstCollectionOfWords);
            var secondAlphabet = alphabetParser.ParseAlphabet(secondCollectionOfWords);

            var firstSet = new StringSet()
            {
                Alphabet = firstAlphabet,
                Set = firstCollectionOfWords,
            };

            var secondSet = new StringSet()
            {
                Alphabet = secondAlphabet,
                Set = secondCollectionOfWords,
            };

            // Return the commonly spelled words found in both sets
            return intersectionFinder.FindIntersectionOfSets(firstSet, secondSet);
        }
    }
}