using System.Collections.Generic;
using TwoDict.Service.DataStructures.Trie;

namespace TwoDict.Service.Services.StringComparison
{
    public struct StringSet
    {
        public HashSet<char> Alphabet { get; set;}

        public IList<string> Set { get; set; }
    }

    public class StringSetIntersectionFinder : IStringSetIntersectionFinder
    {
        // Given two sets of strings, return the intersection of both sets
        public IList<string> FindIntersectionOfSets(StringSet firstSet, StringSet secondSet)
        {
            var intersectionSet = new List<string>();

            var trie = new Trie(firstSet.Alphabet);

            foreach (var value in firstSet.Set)
                trie.Insert(value);

            foreach (var value in secondSet.Set)
                if (trie.IsWordInTree(value))
                    intersectionSet.Add(value);

            return intersectionSet;
        }
    }
}