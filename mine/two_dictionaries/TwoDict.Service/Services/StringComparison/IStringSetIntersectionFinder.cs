using System.Collections.Generic;

namespace TwoDict.Service.Services.StringComparison
{
    public interface IStringSetIntersectionFinder
    {
        IList<string> FindIntersectionOfSets(StringSet firstSet, StringSet secondSet);
    }
}