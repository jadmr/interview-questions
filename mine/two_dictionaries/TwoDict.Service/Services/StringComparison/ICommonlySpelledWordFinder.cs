using System.Collections.Generic;

namespace TwoDict.Service.Services.StringComparison
{
    public interface ICommonlySpelledWordFinder
    {
        IList<string> FindCommonlySpelledWords(IList<string> firstCollectionOfWords, IList<string> secondCollectionOfWords);
    }
}