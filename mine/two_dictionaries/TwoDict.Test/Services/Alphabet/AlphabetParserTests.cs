using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TwoDict.Service.Services.Alphabet;

namespace TwoDict.Test.Services.Alphabet
{
    [Category(nameof(Category.Unit))]
    public class AlphabetParserTests
    {
        [Test]
        public void ParseAlphabet_WithEmptyList_ShouldReturnEmptySet()
        {
            // Arrange
            var values = new List<string>();
            var parser = new AlphabetParser();

            // Act
            var results = parser.ParseAlphabet(values);

            // Assert
            Assert.IsFalse(results.Any());
        }

        [Test]
        public void ParseAlphabet_WithStringList_ShouldReturnSetContainingEveryCharacter()
        {
            // Arrange
            var values = new List<string>()
            {
                "apple",
                "band",
                "car",
                "dog",
                "echo",
            };

            var parser = new AlphabetParser();

            // Act
            var results = parser.ParseAlphabet(values);

            // Assert
            Assert.AreEqual(12, results.Count);

            Assert.IsTrue(results.Contains('a'));
            Assert.IsTrue(results.Contains('b'));
            Assert.IsTrue(results.Contains('c'));
            Assert.IsTrue(results.Contains('d'));
            Assert.IsTrue(results.Contains('e'));
            Assert.IsTrue(results.Contains('g'));
            Assert.IsTrue(results.Contains('h'));
            Assert.IsTrue(results.Contains('l'));
            Assert.IsTrue(results.Contains('n'));
            Assert.IsTrue(results.Contains('o'));
            Assert.IsTrue(results.Contains('p'));
            Assert.IsTrue(results.Contains('r'));
        }
    }
}