using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TwoDict.Service.Services.StringComparison;

namespace TwoDict.Test.Services.StringComparison
{
    [Category(nameof(Category.Unit))]
    public class StringSetIntersectionFinderTests
    {
        [Test]
        public void FindIntersectionOfSets_WithTwoSetsContainingIntersection_ShouldReturnIntersection()
        {
            // Arrange
            var englishSet = new StringSet()
            {
                Alphabet = new HashSet<char>() {'a', 'b', 'c', 'e', 'n', 'o'},
                Set = new List<string>()
                {
                    "bean",
                    "cab",
                    "no",
                    "ocean",
                    "once",
                    "one",
                },
            };

            var spanishSet = new StringSet()
            {
                Alphabet = new HashSet<char>() {'a', 'b', 'c', 'e', 'n', 'o'},
                Set = new List<string>()
                {
                    "acaba",
                    "cena",
                    "con",
                    "no",
                    "once",
                    "nace",
                },
            };

            var intersectionFinder = new StringSetIntersectionFinder();

            // Act
            var intersection = intersectionFinder.FindIntersectionOfSets(englishSet, spanishSet);

            // Assert
            Assert.AreEqual(2, intersection.Count);
            Assert.IsTrue(intersection.Contains("no"));
            Assert.IsTrue(intersection.Contains("once"));
        }

        [Test]
        public void FindIntersectionOfSets_WithTwoSetsContainingNoIntersection_ShouldReturnEmptyList()
        {
            // Arrange
            var englishSet = new StringSet()
            {
                Alphabet = new HashSet<char>() {'a', 'b', 'c', 'e', 'n', 'o'},
                Set = new List<string>()
                {
                    "bean",
                    "cab",
                    "ocean",
                    "one",
                },
            };

            var spanishSet = new StringSet()
            {
                Alphabet = new HashSet<char>() {'a', 'b', 'c', 'e', 'n', 'o'},
                Set = new List<string>()
                {
                    "acaba",
                    "cena",
                    "con",
                    "nace",
                },
            };

            var intersectionFinder = new StringSetIntersectionFinder();

            // Act
            var intersection = intersectionFinder.FindIntersectionOfSets(englishSet, spanishSet);

            // Assert
            Assert.IsFalse(intersection.Any());
        }

        [Test]
        public void FindIntersectionOfSets_WithFirstSetEmpty_ShouldReturnEmptyList()
        {
            // Arrange
            var englishSet = new StringSet()
            {
                Alphabet = new HashSet<char>() {},
                Set = new List<string>() {},
            };

            var spanishSet = new StringSet()
            {
                Alphabet = new HashSet<char>() {'a', 'b', 'c', 'e', 'n', 'o'},
                Set = new List<string>()
                {
                    "acaba",
                    "cena",
                    "con",
                    "nace",
                },
            };

            var intersectionFinder = new StringSetIntersectionFinder();

            // Act
            var intersection = intersectionFinder.FindIntersectionOfSets(englishSet, spanishSet);

            // Assert
            Assert.IsFalse(intersection.Any());
        }

        [Test]
        public void FindIntersectionOfSets_WithSecondSetEmpty_ShouldReturnEmptyList()
        {
            // Arrange
            var englishSet = new StringSet()
            {
                Alphabet = new HashSet<char>() {'a', 'b', 'c', 'e', 'n', 'o'},
                Set = new List<string>()
                {
                    "bean",
                    "cab",
                    "ocean",
                    "one",
                },
            };

            var spanishSet = new StringSet()
            {
                Alphabet = new HashSet<char>() {},
                Set = new List<string>() {},
            };

            var intersectionFinder = new StringSetIntersectionFinder();

            // Act
            var intersection = intersectionFinder.FindIntersectionOfSets(englishSet, spanishSet);

            // Assert
            Assert.IsFalse(intersection.Any());
        }
    }
}