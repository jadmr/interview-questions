using System.Collections.Generic;
using NUnit.Framework;
using TwoDict.Service.DataStructures.Trie;

namespace TwoDict.Test.DataStructures.TrieStructure
{
    [Category(nameof(Category.Unit))]
    public class TrieTests
    {
        #region Test Insert

        [Test]
        public void Insert_WithEmptyString_ShouldNotInsertAnything()
        {
            // Arrange
            var alphabet = new HashSet<char>() { 'b', 'e' };
            var trie = new Trie(alphabet);

            // Act
            trie.Insert(string.Empty);

            // Assert
            Assert.IsNull(trie.Root.Children['b']);
            Assert.IsNull(trie.Root.Children['e']);
        }

        [Test]
        public void Insert_WithWord_ShouldInsertWordIntoTree()
        {
            // Arrange
            var alphabet = new HashSet<char>() { 'b', 'e' };
            var trie = new Trie(alphabet);

            // Act
            trie.Insert("bee");

            // Assert
            Assert.IsTrue(trie.Root.Children['b'].Children['e'].Children['e'].IsEndOfWord);
        }

        [Test]
        public void Insert_WithWordsSharingSamePrefix_ShouldInsertBothWords()
        {
            // Arrange
            var alphabet = new HashSet<char>() { 'b', 'e', 'r', 's'};
            var trie = new Trie(alphabet);

            // Act
            trie.Insert("beer");
            trie.Insert("bees");

            // Assert
            Assert.IsTrue(trie.Root.Children['b'].Children['e'].Children['e'].Children['r'].IsEndOfWord);
            Assert.IsTrue(trie.Root.Children['b'].Children['e'].Children['e'].Children['s'].IsEndOfWord);
        }

        [Test]
        public void Insert_WithWordsSharingSamePrefix_ShouldNotFlagCommonPrefixAsWord()
        {
            // Arrange
            var alphabet = new HashSet<char>() { 'b', 'e', 'r', 's'};
            var trie = new Trie(alphabet);

            // Act
            trie.Insert("beer");
            trie.Insert("bees");

            // Assert
            Assert.IsFalse(trie.Root.Children['b'].Children['e'].Children['e'].IsEndOfWord);
        }

        [Test]
        public void Insert_WithWordsNotSharingSamePrefix_ShouldInsertBothWords()
        {
            // Arrange
            var alphabet = new HashSet<char>() { 'b', 'e', 'r', 's'};
            var trie = new Trie(alphabet);

            // Act
            trie.Insert("seer");
            trie.Insert("bees");

            // Assert
            Assert.IsTrue(trie.Root.Children['b'].Children['e'].Children['e'].Children['s'].IsEndOfWord);
            Assert.IsTrue(trie.Root.Children['s'].Children['e'].Children['e'].Children['r'].IsEndOfWord);
        }

        #endregion

        #region Test IsWordInTree

        [Test]
        public void IsWordInTree_WithEmptyString_ShouldReturnFalse()
        {
            // Arrange
            var alphabet = new HashSet<char>() { 'b', 'e', 'r', 's'};

            var trie = new Trie(alphabet);
            trie.Insert("beer");
            trie.Insert("bees");

            // Act
            var result = trie.IsWordInTree(string.Empty);

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IsWordInTree_WithWordsSharingSamePrefix_ShouldNotReturnCommonPrefix()
        {
            // Arrange
            var alphabet = new HashSet<char>() { 'b', 'e', 'r', 's'};

            var trie = new Trie(alphabet);
            trie.Insert("beer");
            trie.Insert("bees");

            // Act
            var result = trie.IsWordInTree("bee");

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IsWordInTree_WithWordInTrie_ShouldReturnTrue()
        {
            // Arrange
            var alphabet = new HashSet<char>() { 'b', 'e', 'r', 's'};

            var trie = new Trie(alphabet);
            trie.Insert("beer");
            trie.Insert("bees");

            // Act
            var result = trie.IsWordInTree("beer");

            // Assert
            Assert.IsTrue(result);

            // Act
            result = trie.IsWordInTree("bees");

            // Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void IsWordInTree_WithWordNotInTrie_ShouldReturnFalse()
        {
            // Arrange
            var alphabet = new HashSet<char>() { 'b', 'e', 'r', 's'};

            var trie = new Trie(alphabet);
            trie.Insert("beer");
            trie.Insert("bees");

            // Act
            var result = trie.IsWordInTree("seer");

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void IsWordInTree_WithAlphabetNotInTrie_ShouldReturnFalse()
        {
            // Arrange
            var alphabet = new HashSet<char>() { 'b', 'e', 'r', 's'};

            var trie = new Trie(alphabet);
            trie.Insert("beer");
            trie.Insert("bees");

            // Act
            var result = trie.IsWordInTree("wat");

            // Assert
            Assert.IsFalse(result);
        }



        #endregion
    }
}