using System.Collections.Generic;
using NUnit.Framework;
using TwoDict.Service.DataStructures.Trie;

namespace TwoDict.Test.DataStructures.TrieStructure
{
    [Category(nameof(Category.Unit))]
    public class TrieNodeTests
    {
        [Test]
        public void CreateTrieNode_WithAlphabet_ShouldCreateChildrenKeyForEachAlphabetCharacter()
        {
            // Arrange
            var alphabet = new HashSet<char>() {'a', 'b', 'c'};

            // Act
            var node = new TrieNode(alphabet);

            // Assert
            Assert.AreEqual(3, node.Children.Count);
        }

        [Test]
        public void CreateTrieNode_WithAlphabet_ShouldAddEachAlphabetCharacterToChildrenDictionary()
        {
            // Arrange
            var alphabet = new HashSet<char>() {'a', 'b', 'c'};

            // Act
            var node = new TrieNode(alphabet);

            // Assert
            Assert.IsTrue(node.Children.ContainsKey('a'));
            Assert.IsTrue(node.Children.ContainsKey('b'));
            Assert.IsTrue(node.Children.ContainsKey('c'));
        }

        [Test]
        public void CreateTrieNode_WithAlphabet_ShouldCreateChildrenValuesToNull()
        {
            // Arrange
            var alphabet = new HashSet<char>() {'a', 'b', 'c'};

            // Act
            var node = new TrieNode(alphabet);

            // Assert
            Assert.IsNull(node.Children['a']);
            Assert.IsNull(node.Children['b']);
            Assert.IsNull(node.Children['c']);
        }

        [Test]
        public void CreateTrieNode_WithAlphabet_ShouldInitializeIsEndOfWordToFalse()
        {
            // Arrange
            var alphabet = new HashSet<char>() {'a', 'b', 'c'};

            // Act
            var node = new TrieNode(alphabet);

            // Assert
            Assert.IsFalse(node.IsEndOfWord);
        }

        [Test]
        public void ChangeIsEndOfWord_SetToTrue_ShouldSetIsEndOfWordToTrue()
        {
            // Arrange
            var alphabet = new HashSet<char>() {'a', 'b', 'c'};
            var node = new TrieNode(alphabet);

            // Act
            node.IsEndOfWord = true;

            // Assert
            Assert.IsTrue(node.IsEndOfWord);
        }
    }
}