﻿using System;
using System.Collections.Generic;

namespace ExpressionEvaluator
{
    public class ExpressionEvaluator
    {
        public int Evaluate(string expression)
        {
            var values = new Stack<int>();
            var operators = new Stack<char>();

            for (int i = 0; i < expression.Length; i++)
            {
                if (char.IsWhiteSpace(expression[i]))
                    continue;

                // If number
                if (IsNumeric(expression[i]))
                {
                    string number = string.Empty;

                    // Read the whole number, may be more than one digit
                    while (i < expression.Length && IsNumeric(expression[i]))
                    {
                        number += expression[i];
                        i += 1;
                    }

                    values.Push(int.Parse(number));

                    // Rewind since the above look incremented i to the next token
                    i -= 1;
                }
                else if (expression[i] == '(')
                {
                    operators.Push(expression[i]);
                }
                else if (expression[i] == ')')
                {
                    while (operators.Peek() != '(')
                    {
                        var result = PerformOperation(operators.Pop(), values.Pop(), values.Pop());
                        values.Push(result);
                    }

                    operators.Pop(); // pop the starting paranthesis
                }
                else if (expression[i] == '+' || expression[i] == '-' || expression[i] == '*' || expression[i] == '/')
                {
                    while(operators.Count > 0 && DoesSecondOperatorHavePrecedenceOverFirst(expression[i], operators.Peek()))
                    {
                        var result = PerformOperation(operators.Pop(), values.Pop(), values.Pop());
                        values.Push(result);
                    }

                    operators.Push(expression[i]);
                }
            }

            while (operators.Count > 0)
            {
                var result = PerformOperation(operators.Pop(), values.Pop(), values.Pop());
                values.Push(result);
            }

            return values.Pop();
        }

        private bool DoesSecondOperatorHavePrecedenceOverFirst(char firstOperator, char secondOperator)
        {
            if (secondOperator == '(' || secondOperator == ')')
                return false;
            if ((firstOperator == '*' || firstOperator == '/') && (secondOperator == '+' || secondOperator == '-'))
                return false;

            return true;
        }

        private int PerformOperation(char operation, int firstNumber, int secondNumber)
        {
            switch (operation)
            {
                case '+':
                    return firstNumber + secondNumber;
                case '-':
                    return firstNumber - secondNumber;
                case '*':
                    return firstNumber * secondNumber;
                case '/':
                    if (secondNumber == 0)
                        throw new NotSupportedException($"Cannot divide {firstNumber} by 0");
                    return firstNumber / secondNumber;
            }

            throw new NotSupportedException($"Unknown operator: {operation}");
        }

        private bool IsNumeric(char token)
        {
            return token >= '0' && token <= '9';
        }
    }
}
