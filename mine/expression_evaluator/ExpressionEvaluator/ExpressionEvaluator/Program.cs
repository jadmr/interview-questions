﻿using System;

namespace ExpressionEvaluator
{
    class Program
    {
        static void Main(string[] args)
        {
            var expression = "4 - 2 / 2 * 2 + 3";

            var evaluator = new ExpressionEvaluator();
            var result = evaluator.Evaluate(expression);

            Console.WriteLine(result);
        }
    }
}
