﻿using Heaps.Heap;
using System.Collections.Generic;

namespace Heaps.Queue
{
    public class PriorityQueue : MaxHeap
    {
        public int Length { get; set; }

        public int GetMax(List<int> values)
        {
            return values[0];
        }

        public int PopMax(List<int> values)
        {
            if (Length == 0)
                return -1;

            var max = values[0];
            values[0] = values[Length - 1];
            Length -= 1;

            Heapify(values, 0, Length);

            return max;
        }
    }
}
