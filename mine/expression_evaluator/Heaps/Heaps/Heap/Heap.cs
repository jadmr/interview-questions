﻿using System.Collections.Generic;

namespace Heaps.Heap
{
    public abstract class Heap
    {
        public void BuildHeap(List<int> heap)
        {
            // Leaf nodes are N/2+1, N/2+2, N/2+3, etc. Begin heapify process at the parents of the leaf nodes
            for (int i = heap.Count / 2 - 1; i >= 0; i--)
            {
                Heapify(heap, i, heap.Count);
            }
        }

        public abstract void Heapify(List<int> heap, int index, int size);
    }
}
