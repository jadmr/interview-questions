﻿using System.Collections.Generic;

namespace Heaps.Heap
{
    public class MinHeap : Heap
    {
        public override void Heapify(List<int> heap, int index, int size)
        {
            var left = 2 * index + 1;
            var right = 2 * index + 2;

            var smallest = index;

            if (left < size && heap[left] < heap[smallest])
                smallest = left;

            if (right < size && heap[right] < heap[smallest])
                smallest = right;

            if (smallest != index)
            {
                var temp = heap[smallest];
                heap[smallest] = heap[index];
                heap[index] = temp;

                Heapify(heap, smallest, size);
            }
        }
    }
}
