﻿using System.Collections.Generic;

namespace Heaps.Heap
{
    public class MaxHeap : Heap
    {
        public override void Heapify(List<int> heap, int index, int size)
        {
            var left = 2 * index + 1;
            var right = 2 * index + 2;

            var largest = index;

            if (left < size && heap[left] > heap[largest])
                largest = left;

            if (right < size && heap[right] > heap[largest])
                largest = right;

            if (largest != index)
            {
                var temp = heap[largest];
                heap[largest] = heap[index];
                heap[index] = temp;

                // Continue at the swaped location (down the tree)
                Heapify(heap, largest, size);
            }
        }
    }
}
