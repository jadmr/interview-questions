﻿using Heaps.Heap;
using Heaps.Sorting;
using System;
using System.Collections.Generic;

namespace Heaps
{
    class Program
    {
        static void Main(string[] args)
        {
            var maxHeap = new MaxHeap();
            var minHeap = new MinHeap();

            Console.WriteLine("Ascending order:");

            var values = new List<int>() { 4, 7, 2, 6, 10, 1, 8, 9, 3, 5 };
            var sorter = new HeapSort(maxHeap);
            sorter.Sort(values);

            foreach (var element in values)
                Console.Write($"{element}, ");

            Console.WriteLine("\nDescending order:");

            values = new List<int>() { 4, 7, 2, 6, 10, 1, 8, 9, 3, 5 };
            sorter = new HeapSort(minHeap);
            sorter.Sort(values);

            foreach (var element in values)
                Console.Write($"{element}, ");
        }
    }
}
