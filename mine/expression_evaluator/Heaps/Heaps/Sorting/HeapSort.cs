﻿using System.Collections.Generic;

namespace Heaps.Sorting
{
    class HeapSort
    {
        private readonly Heap.Heap heap;

        public HeapSort(Heap.Heap heap)
        {
            this.heap = heap;
        }

        public void Sort(List<int> values)
        {
            var size = values.Count;
            heap.BuildHeap(values);

            for (int i = values.Count - 1; i >= 0; i--)
            {
                // Swap head with last element
                var temp = values[0];
                values[0] = values[i];
                values[i] = temp;

                // Leave the last element alone for next iteration
                size -= 1;

                // Re-heapify
                heap.Heapify(values, 0, size);
            }
        }
    }
}
