/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class Solution {
    public ListNode ReverseList(ListNode head)
    {
        if (head == null)
            return null;
        
        if (head.next == null)
            return head;
        
        ListNode newHead = null;
        ListNode nextNode = new ListNode(head.val, null);
        
        head = head.next;
        while (head != null)
        {
            var node = new ListNode(head.val, nextNode);
            nextNode = node;
            newHead = node;
            
            head = head.next;
        }
        
        return newHead;
    }
}