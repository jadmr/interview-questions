using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var nums = new int[5] { 2,1,0,3,12 };

            MoveZeros(nums);

            foreach (var num in nums)
                Console.Write($"{num},");

            Console.WriteLine();
        }

        private static void MoveZeros(int[] nums)
        {
            int numZeros = 0;
            int j = 0; // Tracks next swap position

            // Find the first zero
            for (int i = 0; i < nums.Length; i++)
                if (nums[i] == 0)
                {
                    j = i;
                    break;
                }

            for (int i = j; i < nums.Length; i++)
            {
                if (nums[i] == 0)
                {
                    numZeros++;
                    continue;
                }

                // The value is not zero
                nums[j] = nums[i];
                j++;
            }

            for (int i = nums.Length - numZeros; i < nums.Length; i++)
            {
                nums[i] = 0;
            }
        }
    }
}

