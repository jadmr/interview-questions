/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class Solution {
    public ListNode RemoveNthFromEnd(ListNode head, int n)
    {
        int count = 0;
        ListNode current = head;
        ListNode previous = null;
        ListNode nthNode = null;

        while (current != null)
        {
            if (count % n == 0)
            {
                nthNode = previous;
            }

            previous = current;
            current = current.next;
            count++;
        }

        // if head
        if (nthNode == head)
        {
            head = head.next;
            return head;
        }

        // if tail
        if (nthNode.next == null)
        {
            previous.next = null;
            return head;
        }

        // else if in between
        nthNode.val = nthNode.next.val;
        nthNode.next = nthNode.next.next;
        return head;
    }
}