public class Solution {
    public bool IsValidSudoku(char[][] board) 
    {
        // Check rows
        foreach (var row in board)
            if (!IsRowValid(row))
                return false;
        
        // Check columns
        for (int i = 0; i < board.Length; i++)
            if (!IsColumnValid(board, i))
                return false;
        
        // Check quadrants
        if (!IsQuadrantValid(board, 0, 2, 0, 2))
            return false;
        if (!IsQuadrantValid(board, 3, 5, 0, 2))
            return false;
        if (!IsQuadrantValid(board, 6, 8, 0, 2))
            return false;
        if (!IsQuadrantValid(board, 0, 2, 3, 5))
            return false;
        if (!IsQuadrantValid(board, 3, 5, 3, 5))
            return false;
        if (!IsQuadrantValid(board, 6, 8, 3, 5))
            return false;
        if (!IsQuadrantValid(board, 0, 2, 6, 8))
            return false;
        if (!IsQuadrantValid(board, 3, 5, 6, 8))
            return false;
        if (!IsQuadrantValid(board, 6, 8, 6, 8))
            return false;
        
        return true;
    }
    
    private bool IsQuadrantValid(char[][] board, int rowStart, int rowEnd, int colStart, int colEnd)
    {
        var map = new HashSet<char>();
        
        for (int i = rowStart; i < rowEnd; i++)
        {
            for (int j = colStart; j < colEnd; j++)
            {
                var cell = board[i][j];
                
                if (cell == '.')
                    continue;
                
                if (map.Contains(cell))
                    return false;
                
                map.Add(cell);
            }
        }
        
        return true;
    }
    
    private bool IsRowValid(char[] row)
    {
        var map = new HashSet<char>();
        
        foreach (var cell in row)
        {
            if (cell == '.')
                continue;
            
            int number;
            var success = Int32.TryParse(cell.ToString(), out number);
            if (success)
                if (number < 1 || number > 9)
                    return false;
            else
                return false;
            
            if (map.Contains(cell))
                return false;
            
            map.Add(cell);
        }
        
        if (map.Count == 0)
            return false;
        
        return true;
    }
    
    private bool IsColumnValid(char[][] board, int column)
    {
        var map = new HashSet<char>();
        
        foreach (var row in board)
        {
            var cell = row[column];
            
            if (cell == '.')
                continue;
            
            int number;
            var success = Int32.TryParse(cell.ToString(), out number);
            if (success)
                if (number < 1 || number > 9)
                    return false;
            else
                return false;
            
            if (map.Contains(cell))
                return false;
            
            map.Add(cell);
        }
        
        if (map.Count == 0)
            return false;
        
        return true;
    }
}