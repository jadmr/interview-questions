public class Solution {
    private class CharCount {
        public int Count {get; set;}
        public int Position {get; set;}
        
        public CharCount(int c, int p) {
            Count = c;
            Position = p;
        }
    }
    
    public int FirstUniqChar(string s) {
        var map = new Dictionary<char, CharCount>();
        
        for (int i = 0; i < s.Length; i++) {
            var character = s[i];
            
            if (!map.ContainsKey(character))
            {
                map.Add(character, new CharCount(1, i));
                continue;
            }
            
            map[character].Count += 1;
        }
        
        int minPosition = Int32.MaxValue;
        foreach (var val in map.Values) {
            if (val.Count > 1)
                continue;
            
            if (val.Position < minPosition)
                minPosition = val.Position;
        }
        
        return minPosition == Int32.MaxValue ? -1 : minPosition;
    }
}
