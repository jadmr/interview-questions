public class Solution {
    public bool IsPalindrome(string s) {
        var text = string.Empty;
        
        foreach (var val in s) {
            if (!Char.IsLetterOrDigit(val))
                continue;
            
            text += Char.ToLower(val);
        }
        
        int end = text.Length - 1;
        for (int start = 0; start < text.Length; start++, end--) {
            if (start == end)
                return true;
            
            if (text[start] != text[end])
                return false;
        }
        
        return true;
    }
}
