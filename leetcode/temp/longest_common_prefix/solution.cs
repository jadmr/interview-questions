public class Solution {
    public class TrieNode
    {
        public char? Character { get; set;}
        
        public int TimesCharVisited {get; set;}
        
        public Dictionary<char, TrieNode> Map {get; set;}
        
        public TrieNode(char? character) 
        {
            Character = character;
            TimesCharVisited = 1;
            Map = new Dictionary<char, TrieNode>();
        }
    }
    
    public string LongestCommonPrefix(string[] strs) 
    {
        // Make a trie. At each level, record how many times that level (char) was visited at insertion
        
        // Check the root of the trie. If there are multiple non-null children, no common prefix
        
        // Otherwise, do a dfs. Continue DFS until child's count != roots. This will be the LCP.
    }
    
    private void Insert(string word, TrieNode node)
    {
        // If node contains child, increment
    }
}