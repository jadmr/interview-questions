/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    public void DeleteNode(ListNode node) 
    {
        // Copy the next node's value into this one
        node.val = node.next.val;
        
        // Delete the next node
        if (node.next.next == null)
        {
            // If next node is tail, set next to null
            node.next = null;
            return;
        }
        
        // Else, remove it
        node.next = node.next.next;
    }
}