public class Solution {
    public class CharCount {
        public int SCount {get;set;}
        
        public int TCount {get;set;}
        
        public CharCount(int sCount, int tCount) {
            SCount = sCount;
            TCount = tCount;
        }
    }
    
    public bool IsAnagram(string s, string t) {
        if (s.Length != t.Length)
            return false;
        
        var map = new Dictionary<char, CharCount>();
        
        foreach (var val in s) {
            if (!map.ContainsKey(val)) {
                map.Add(val, new CharCount(1, 0));
                continue;
            }
                
            map[val].SCount++;
        }
        
        foreach (var val in t) {
            if (!map.ContainsKey(val))
                return false;
            
            map[val].TCount++;
        }
        
        foreach (var key in map.Keys) {
            var val = map[key];
            
            if (val.SCount != val.TCount)
                return false;
        }
        
        return true;
    }
}
