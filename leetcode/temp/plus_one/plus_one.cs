using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var number = new int[3] { 1,2,9 };
            var increment = 1;

            var answer = AddNumberToArray(number, increment);

            foreach (var num in answer)
                Console.Write(num);

            Console.WriteLine();
        }

        private static int[] AddNumberToArray(int[] number, int value)
        {
            List<int> answer = new List<int>();
            int carry = value;

            int i = number.Length - 1;
            while (i >= 0 || carry != 0)
            {
                if (i < 0 && carry != 0)
                {
                    answer.Insert(0, carry);
                    break;
                }

                var sum = number[i] + carry;

                if (sum < 10)
                {
                    carry = 0;
                    answer.Insert(0, sum);
                }
                else if (sum == 10)
                {
                    carry = 1;
                    answer.Insert(0, 0);
                }
                else
                {
                    carry = sum % 10;
                    answer.Insert(0, sum - carry);
                }

                i--;
            }

            return answer.ToArray();
        }
    }
}
