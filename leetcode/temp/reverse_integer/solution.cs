public class Solution {
    public int Reverse(int x) {
        if (x == 0)
            return 0;
        
        var tempX = x.ToString();

        bool isNegative = false;
        string answer = "";
        for (int i = tempX.Length - 1; i >= 0; i--) {
            var val = tempX[i];
            
            // Strip negative sign
            if (val == '-') {
                isNegative = true;
                continue;
            }
            
            answer += val;
        }
        
        // Having been reversed, strip all beginning zeroes
        int count = 0;
        while (count < answer.Length) {
            var val = answer[count];
            
            if (val != '0')
                break;
            
            count++;
        }
        
        if (count > 0)
            answer = answer.Remove(0, count);
        
        // Check the bounds by converting to Int64
        var boundsCheckVal = Int64.Parse(answer);
        
        if (isNegative)
            boundsCheckVal *= -1;
        
        if (boundsCheckVal > Int32.MaxValue || boundsCheckVal < (-1 * Int32.MaxValue))
            return 0;
        
        return isNegative ? Int32.Parse(answer) * -1 : Int32.Parse(answer);
    }
}
