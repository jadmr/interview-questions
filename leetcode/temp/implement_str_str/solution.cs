public class Solution {
    public int StrStr(string haystack, string needle) {
        if (string.IsNullOrWhiteSpace(needle))
            return 0;
        
        if (string.IsNullOrWhiteSpace(haystack))
            return -1;
        
        if (needle.Length > haystack.Length)
            return -1;
        
        var positions = new List<int>();
        for (int i = 0; i < haystack.Length; i++)
            if (haystack[i] == needle[0])
                positions.Add(i);
        
        var minPos = Int32.MaxValue;
        foreach (var pos in positions)
            if (IsInHaystack(haystack, needle, pos))
                if (pos < minPos)
                    minPos = pos;
        
        return minPos != Int32.MaxValue ? minPos : -1;
    }
    
    private bool IsInHaystack(string haystack, string needle, int pos) {
        for (int i = pos; i < haystack.Length && i < needle.Length; i++)
            if (haystack[i] != needle[i])
                return false;
        
        return true;
    }
}
