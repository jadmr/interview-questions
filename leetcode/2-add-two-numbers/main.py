"""
Leetcode solution
"""
import sys

# pylint: disable=too-few-public-methods
# pylint: disable=redefined-builtin
class ListNode:
    """
    Node in linked list
    """
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=invalid-name
class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        carry = 0
        answer_head: ListNode = None
        previous_answer: ListNode = answer_head

        first_current = l1
        second_current = l2

        count = 0
        while first_current is not None or second_current is not None or carry != 0:
            first_num = 0 if first_current is None else first_current.val
            second_num = 0 if second_current is None else second_current.val

            print(f"DEBUG: first_num: {first_num}")
            print(f"DEBUG: second_num: {second_num}")
            print(f"DEBUG: carry: {carry}")

            current_sum = first_num + second_num + carry
            value = 0

            print(f"DEBUG: current_sum: {current_sum}")

            if current_sum > 9:
                value = current_sum - 10
                carry = 1
            else:
                value = current_sum
                carry = 0

            answer_node: ListNode = ListNode(value)

            if count == 0:
                answer_head = answer_node
            else:
                previous_answer.next =  answer_node

            previous_answer = answer_node

            # advance to the next values in the linked lists
            if first_current is not None:
                first_current = first_current.next
            if second_current is not None:
                second_current = second_current.next
            count += 1

        return answer_head

def main():
    """
    Entry point for solution
    """
    args = sys.argv[1:]

    first_head: ListNode = None
    second_head: ListNode = None

    if not args:
        first_head = construct_linked_list([9,9,9,9,9,9,9])
        second_head = construct_linked_list([9,9,9,9])

    print("Arguments received:", args)

    print(f"First list: {convert_list_to_str(first_head)}")
    print(f"Second list: {convert_list_to_str(second_head)}")

    solution = Solution()
    answer = solution.addTwoNumbers(first_head, second_head)

    print( convert_list_to_str(answer))


def construct_linked_list(num_list) -> ListNode:
    """
    Creates a linked list from the list
    """
    head: ListNode= ListNode(num_list[0])
    previous: ListNode = head

    if num_list.count == 1:
        return head

    for num in num_list[1:]:
        node = ListNode(num)

        previous.next = node
        previous = node

    return head

def convert_list_to_str(num_list) -> str:
    """
    Prints the linked list
    """

    str_val: str = ""
    current = num_list

    while current is not None:
        str_val += str(current.val)

        if current.next is not None:
            str_val += ", "

        current = current.next

    return f"[{str_val}]"

if __name__ == "__main__":
    main()
