#!/bin/bash

# Check if folder name is provided
if [ -z "$1" ]; then
	echo "Usage: $0 <folder_name>"
	exit 1
fi

FOLDER_NAME="$1"

mkdir -p "$FOLDER_NAME"

cp ./template/main.py "$FOLDER_NAME"/

echo "Folder '$FOLDER_NAME' created and main.py copied."
